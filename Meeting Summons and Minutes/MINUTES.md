# Minutes February 21 (group meeting)

Attendance: Everyone
Referent: Britt-Åse

#1 Planning
Discussion on how we were going to approach the project that was going to be released the next week.

# Minutes February 28 (group meeting)

Attendance: Everyone
Referent: Even

### 1. Customer: 
Discussion of who our customer will be. Conclusion: our customer will be a squash team who can use the software to organize tournaments and matches. NB! This is dependent on the squash team’s willingness; if they are not willing, we will find a different customer. 

### 2. Vision doc: 
Discussing and establishing a vision doc for the project. We talked about the data structure and functionality of the project. A more thorough execution of the design doc must depend on the demands of the customer, which we did not yet have, but a preliminary prototyping will take place, based on basic principles of tournament organization.  

### 3. Domain model: 
discussion of domain model, which software to use for creating it? 

### 4. Usability test: 
conducting a usability test with the customer? A working prototype must be completed first 

### 5. Meeting with TA: 
Britt Åse is to contact TA, establish how/where they wish to contact us. 

### 6. Delegation of tasks: 
completing the prototype, further progress on the vision document and establishing contact with the customer and the TA was discussed, and tasks sucsessfully delegated. See in task list in Project outline document.


# Minutes March 7 (group meeting)

Attendance: Everyone
Referent: Markus

#### 1. Plan for the day
Discuss what we want to add to the second iteration of the project.


#### 2. What have we done:
* We went through the user test rapport to see what we are missing from the second iteration
* We also looked at what we would change now when we have the feedback from our users.
* Discussed what we would inlucde in the second iteration of our project.
* Discoussed how our tournament would work, wether it be a bracket tournament or not.

### 3. What have we planned
* What we would ask the user.
*Does the user want to decide how many plaers are a part of the tournament?
* Does the user Want to have a set amount or does the user want to change the amount of players for each tournament?

### 4. What happens next week:
*We are meeting up at 08:15 to create the pseudo code for our product before the client meeting with Ali.

Remember to create issue boards and push to git. 


# Minutes March 14 (group meeting)

Attendance: Everyone
Referent: Åsulv

#### 1. Plan for the day
Write pseudocode for MVP together, before client meeting at 11:20.


#### 2. Plan for the next two weeks (before March 25)
Second iteration hand in March 25, must include (delegation of tasks): 
* MVP (Åsulv)
* User test (Sara)
* Sequence diagram
* Improved wireframe (prototype for full program – more detailed than the MVP) (Even)
* Vision document (Britt-Åse and Markus)
* First version of Wiki
* Log (Everyone)
* Gantt chart (Markus)

Remember to create issue boards and push to git. 


# Minutes March 14 (client meeting)

Attendance: Everyone
Referent: Sara

#### 1. Feedback on first iteration and the work so far
Meeting summon (email) must contain agenda. 

**Vision document**
* We have moved too far from the Vision Document. 
* Too short - we need to work more on it. 
* "Main features" are/means the main headings - all points should be described. 
* Needs a risk table which lists:
   1. Risks
   2. Consequences
   3. Mitigation (what do we do if a risk occurs)
   4. Probability (how likely is it that the risks occurs)
   5. Importance (how important/damaging is the risk)
   * Probability and importance are determined through ranking (by everyone in the group), to find the "average". 

**Wireframe**
* Needs the ability to "edit" (we have already started to alter the code/program, but keep it in mind). 

**User test**
* User tests should not include name of test person. However, age/age group, gender and experience with software/squash tournament should be included. 
* Test on MORE users, specifically users that are not experienced with software. 

**Git**
* User the issue board (everybody must create issues, and use the issue board).
* And in general, use git more. 

#### 2. Other issues
* Remember that this is a software development course so the focus is on the DOCUMENTATION not on the programming.  





# Minutes March 17 (TA meeting)

Attendance: Everyone
Referent: Sara

#### 1. Feedback on first iteration (from TA, Camilla)
**Vision Document:** 
* To the point and easy to navigate.
* We need to write more in the Vision Document to avoid a heavy workload the last weeks. 
    * Designer part: Replace «group 12» with all our names (and do not write “designers” in the start). 

**User test:**
* Seems like we got a lot out of the user test. For future user tests: Remember to document the plans/changes you make based on the user test. 

**Domain model:** 
* Over all, good and clear, but a few changes should be made:
     1. Remove functions. (The handed in domain model looks a little bit like a class diagram. The class diagram will be similar to the domain model, but it should contain the classes created in the code etc.)
     2. Correct the multiplicity numbers (always a whole number, since it means “at least ‘x’”, not 1…*).


#### 2. Questions on the second iteration
Can we add features to the MVP that are not "bare minimum"?
* Answer: Prioritize the most important basic features so that it runs bug-free. But we are allowed to add more than that if we want. 


#### 3. Other issues
Rescheduling weekly TA meetings to 08.15 (every Thursday). 

# Minutes March 21 (group meeting)

Attendance: Everyone

Referent: Åsulv

#### 1. Update: What have been done since last time?
Wireframe: Almost completely done.

MVP: Not completely done, there is a bug in the code (choice of matchup in a bracket).

Vision doc: Britt-Åse is working on it. A bit unsure on where to place things - asking Camilla TA for help.

#### 2. Further?
Vision doc: Britt-Åse and Markus continues working on it, WITH risk table (see the meeting minutes from the client meeting).

Gantt: Markus is looking into it.

MVP: Åsulv continues, with help from Even.

Domain model: Markus creates it (should be done quickly).

The user test: Sara makes the user test and the user cases.
- If there is time: Use the new wireframe-test on the new test users.

Wiki: Sara starts working on it.

Sequence diagram: Britt-Åse and Markus look into this when they have time, unless anyone else have time for it.

#### 3. Other:
Screenshot of the issue board: Do it often.
-  Do we have a screenshot from the previous GitLab issue board?

A new meeting is setup on Wednesday at 13:15, digitally.

Remember to ask Camilla for help.

Åsulv is responsible for meetings this week: The person responsible for meetings must send the meeting agenda.

# Minutes March 28

Attendance: everyone
Reference: Britt-Åse

#1 Update
What has been done, what needs to be done and by who

# Minutes April 4 (group meeting)

Attendance: Everyone
Referent: Even

#### 1. Plan for the day
Review 2nd iteration feedback

Vision document 2nd draft
It is a little bare boned, but content wise you are on the right track. The specific features listed in result goals would fit better under product features. Result goals is more a general description of your planned product. 

Time log
Log of hours is formatted correctly, but the status report could be displayed a little cleaner. For instance, have a designated box the tasks worked on or include a separate document for status reports.

Gannt chart
Looks good!

Use-Case diagram
The diagram has some uses of include and extend that seem incorrect, for instance “Delete tournament” have an include-arrow to “Create tournament”. I think you can clean up the diagram a little by double checking if certain arrows are needed.

Sequence diagram
For the different alternatives you’ve listed the option and the action together, but actions should be placed on the arrows. And remember to add the activation box to your objects and that responses are with a dotted arrow.

Updated domain model
There are some multiplicity numbers that needs to be changed to “1..1” and some arrows missing.

#### 2. What have we done/what happened/what we talked about:
* We have discouseed the feedback we got from our TA
* We were missing several parts of our documents and there were many key parts of which we had not included. We discoussed why and how to prevent this in the future.

### 3. What have we planned
* We decided that we are far behind schechule and that we neeed to speed up our proccess.
* We planned to have the parts that were missing fixed before the end of easter.


### 4. What happens next week(s)/session:
*The next meeting will be on the 7/04/2022 08:15

# Minutes April 7 (TA meeting)

Attendance: Everyone
Referent: Even

#### 1. Plan for the day
Talk with our TA about the feedback.


#### 2. What have we done/what happened/what we talked about:
* Talked about the feedback we had gotten from our TA and how we could fix the problems.

### 3. What have we planned
* We figured out what we were missing after our talk witht the TA, we therefor decided to divivde the remaining tasks and keep people updated.


### 4. What happens next week(s):
*The next meeting will be after easter on the 7/04/2022 14:40

# Minutes April 7 (Client meeting)

Attendance: Everyone
Referent: Even

#### 1. Plan for the day
Talk with our Client about the MVP.


#### 2. What have we done/what happened/what we talked about:
* We had some techincal problems that accoured so we mananged to waste 10 minutes on finding the correct files.
* We reciveced good feedback on our MVP and our Client recommened that we do not make the code overly glamerous 

### 3. What have we planned
* We planned to have a meeting the day after (8.04.2022), because we had alot of planning to do. 

### 4. What happens next week(s):
*The next meeting will be after easter on the 8/04/2022 08:15

# Minutes April 8 (group meeting)

Attendance: Everyone
Referent: Even

#### 1. Plan for the day
Figure out what we should do during and after easter.


#### 2. What have we done/what happened/what we talked about:
* Figured out what to do during and after easter.
* Divided the remaining tasks

### 3. What have we planned
* We decided that we should be done with the vision doc on the last day of easter. Since we had a huge workload and we were behind schecule


### 4. What happens next week(s):
*The next meeting will be after easter on the 21/04/2022 08:15

# Minutes April 21st (group meeting)

Attendance: Everyone
Referent: Sara

#### 1. Status report: What have we done since last week?

Markus/Britt-Åse:
* Gantt chart and domain model just needs some last changes. 
* The vision document is soon done, but we some last questions for the TA on the meeting later today.

Åsulv: 
* The matchup-bug is fixed. Ability to read and write from file, and randomize matchup has been added. It is finished, but needs to be thoroughly tested. Is uploaded to git repository as zip-file. 

Even: 
* The main report is in progress, focusing on theory.

Sara:
* Wrote on the Vision Document.
* Finished the use case and sequence diagram.

#### 2. Plan for the last week

Individual work:
* Markus and Britt-Åse will finish the last sections of the Vision Document. 
* Sara will run the user tests and write report. Reporting back with the results to make the needed changes on the source code. 
* Even continues to write the main report.
* Åsulv writes the manual

Everyone needs to: 
* Study the source code and test the program to get familiar with it. 
* Contribute to the main report, specifically chapter 3, 4 and 5. 
* Fill in the WIKI page.


#### 3. Anything else?

Questions for TA:
* Several sections in the Vision Document
* Requirements for risiko analysis. 
* Links in WIKI.


# Minutes April 21 (TA meeting)

Attendance: Everyone
Referent: Sara

#### 1. Q&A – Vision Document

Vision document, sections: 
* Section 11.3-6: These small sections must be coherent with each other. 
* 'Target release': Must include the deadlines, from prototype, MVP, to end product. 
* 'Stability': Should include all features that will be included in the end product, and which features are likely to be changed. Discuss how we have kept to the plan for the program.  
* 'Status': Put all features in one table.
* Section 9.4: Briefly describe what types of units (hardware) the program requires (physical requirements). 
* 'Risk analysis': Needs just one, updated, table with risks. 

WIKI:
* Link to gitlab-documents. 

#### 2. The requirements/changes for the last delivery

Requirements (that are still in progress/needs to be done):
* **User Manual:** Should include a brief guide for how to use the program, and how to navigate it, going through all the features, and the according commands. Base the manual own the use case diagram.
* **Main report:** The theory part should include all the theory we have used. The main report is a summary of the relevant parts of the syllabus, methods and results.
* **Standards/Universal design:** WCAG is already explained in Vision doc, but should also be discussed in main report.   
* **Referances:** Main report does not need references for exact pages.
* **Use case:** Add a few more actions if there is time.
* **Persistence:** Explains how data is written and read from file (dta-filer), and the format of the files. 
* **Testing:** Use the template from PROG1003, and test the features of the program. 

OBS: 
* Git: Make sure everything is uploaded to gitlab. Put everything in directories. Update the README-file with contact information.  


# Minutes April 25 (group meeting)

Attendance: Everyone

Referent: Åsulv

### 1. Plan for the day
Each group member gives a status report on the progress on the final iteration. Then plan what work to do for the final week of the project. Plan the final meeting before hand-in.

### 2. What have we done:
* Went through everything that has been done and everything that has to be done before the hand-in on Friday
* Distributed the remaining tasks among the group members
* Planned final meetings

### 3. What have we planned:
* A group meeting on Friday to finish up the work and hand in the project and start work planning the presentation next week.
* A group meeting on Tuesday next week at 12:00 to prepare for the presentation later that day.

### 4. What happens next week:
* Presentation of the project with Ali, tuesday May 3rd 15:00

# Minutes April 28 (weekly group meeting with TA)

Attendance: Everyone

Referent: Åsulv

### 1. Plan for the day:
Ask questions to the TA about the final iteration

### 2. What have we done:

### 3. What have we planned:

# Minutes April 29 (final group meeting)

Attendance: Everyone

Referent: Åsulv

### 1. Plan for the day:
Finish up the project and hand it in on Inspera and begin work on planning the presentation.

### 2. What have we done:
* Finalizing the main report and vision document.
* Uploaded and organized meeting summons and minutes on GitLab.
* Finalizing the Gantt chart.
* Handed in the project on Inspera.
* Made PowerPoint presentations for the presentation next week.

### 3. What have we planned:

### 4. What happens next week:
* Presentation of the project with Ali, tuesday May 3rd 15:00
