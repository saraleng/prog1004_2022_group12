# Summon group meeting Mach 28

To: All group member
Time and date: February 28th, 14:15
Place: this week's meeting will be held on Discord

**Agenda**

1. Discussion of the client
2. Delagation of tasks
3. Establishing contact with TA

Contact this week's meeting organizer if you have any additional points for the agenda, and give notice as soon as possible if you are unable to participate. 

Even
Meeting organizer week 9



# Summon group meeting March 7  

To: All group members 
Time and date: March 7th, 14:15 am 
Place: A159

This weeks meetings will be held at campus. 
 
**Agenda**
1. Plan for today's work
2. Plan for the next iteration 
3. Other issues

Contact this week's meeting organizer if you have any additional points for the agenda, and give notice as soon as possible if you are unable to participate. 
  
Markus
Meeting organizer week 10



# Summon group meeting March 14  

To: All group members 
Time and date: March 14th, 8:15 am 
Place: K110

This weeks meetings will be held at campus in conjunction with collaborative work on the pseudo code for the MVP, and the first client meeting with Ali. We will briefly discuss:
 
**Agenda**
1. Plan for today's work 
2. Plan for the next two weeks (before second iteration hand-in, March 25)
3. Other issues

Contact this week's meeting organizer if you have any additional points for the agenda, and give notice as soon as possible if you are unable to participate. 
  
 
Sara 
Meeting organizer week 11 



# Meeting invitation (client meeting) March 14

*This invitation was sent to Ali (client) via email*

Hi Ali,
 
On behalf of group 12, it is a pleasure to invite you to our first meeting on Monday March 14th at 11:20 - 11:35, in room A132.
 
We will present what we have done so far and discuss potential alterations for the second iteration.
 
Hope this works for you!
 
 
Kind regards,
 
Sara
Group 12



# Summon group meeting March 21

To: All group members
Time and date: March 21st, 02:15 pm
Place: A270

This weeks meeting will be held at campus.

**Agenda**
1. Status report on how the work is going
2. Plan the rest of the work needed for the second assignment
3. Distribute the final tasks among the group members

Contact this week's meeting organizer if you have any additional points for the agenda, and give notice as soon as possible if you are unable to participate.

Remember, the second iteration is due March 25th!


Åsulv
Meeting organizer week 12

# Minutes April 4 (group meeting)

Attendance: Everyone
Referent: Even

#### 1. Plan for the day
Review 2nd iteration feedback

Vision document 2nd draft
It is a little bare boned, but content wise you are on the right track. The specific features listed in result goals would fit better under product features. Result goals is more a general description of your planned product. 

Time log
Log of hours is formatted correctly, but the status report could be displayed a little cleaner. For instance, have a designated box the tasks worked on or include a separate document for status reports.

Gannt chart
Looks good!

Use-Case diagram
The diagram has some uses of include and extend that seem incorrect, for instance “Delete tournament” have an include-arrow to “Create tournament”. I think you can clean up the diagram a little by double checking if certain arrows are needed.

Sequence diagram
For the different alternatives you’ve listed the option and the action together, but actions should be placed on the arrows. And remember to add the activation box to your objects and that responses are with a dotted arrow.

Updated domain model
There are some multiplicity numbers that needs to be changed to “1..1” and some arrows missing.

#### 2. What have we done/what happened/what we talked about:
* We have discouseed the feedback we got from our TA
* We were missing several parts of our documents and there were many key parts of which we had not included. We discoussed why and how to prevent this in the future.

### 3. What have we planned
* We decided that we are far behind schechule and that we neeed to speed up our proccess.
* We planned to have the parts that were missing fixed before the end of easter.


### 4. What happens next week(s)/session:
*The next meeting will be on the 7/04/2022 08:15

# Meeting inviation (client meeting) April 5
*This invitation was sent to Ali via email*

Hello,

I have the pleasure of inviting you to our client meeting on April 7th, at 15.05 in A232.
We will be covering the contents of our second iteration for the project, in particular:
The MVP
The product's sequence diagram
The  improved wireframe
First version of the the project's wiki page
The project's gant chart
If time allows, we would also like to discuss your recommendations for the final iteration, including updates to the software and the vision document.

On behalf of group 12,
Even Stetrud

# Minutes April 7 (TA meeting)

Attendance: Everyone
Referent: Even

#### 1. Plan for the day
Talk with our TA about the feedback.


#### 2. What have we done/what happened/what we talked about:
* Talked about the feedback we had gotten from our TA and how we could fix the problems.

### 3. What have we planned
* We figured out what we were missing after our talk witht the TA, we therefor decided to divivde the remaining tasks and keep people updated.


### 4. What happens next week(s):
*The next meeting will be after easter on the 7/04/2022 14:40

# Minutes April 7 (Client meeting)

Attendance: Everyone
Referent: Even

#### 1. Plan for the day
Talk with our Client about the MVP.


#### 2. What have we done/what happened/what we talked about:
* We had some techincal problems that accoured so we mananged to waste 10 minutes on finding the correct files.
* We reciveced good feedback on our MVP and our Client recommened that we do not make the code overly glamerous 

### 3. What have we planned
* We planned to have a meeting the day after (8.04.2022), because we had alot of planning to do. 

### 4. What happens next week(s):
*The next meeting will be after easter on the 8/04/2022 08:15

# Minutes April 8 (group meeting)

Attendance: Everyone
Referent: Even

#### 1. Plan for the day
Figure out what we should do during and after easter.


#### 2. What have we done/what happened/what we talked about:
* Figured out what to do during and after easter.
* Divided the remaining tasks

### 3. What have we planned
* We decided that we should be done with the vision doc on the last day of easter. Since we had a huge workload and we were behind schecule


### 4. What happens next week(s):
*The next meeting will be after easter on the 21/04/2022 08:15

# Summon group meeting April 21st  

To: All group members 
Time and date: April 21st, 8:15 am 
Place: Digitally

This weeks meeting will be held before meeting with the TA, to get a status report and plan the last few weeks in detail.
 
**Agenda**
1. Status report 
2. Plan for the last week (final deadline, April 29th)
3. Other issues

Remember to write down all questions you have for Camilla (TA).

Contact this week's meeting organizer if you have any additional points for the agenda, and give notice as soon as possible if you are unable to participate. 
  
 
Sara 
Meeting organizer week 16


# Summon group meeting April 25

To: All group members
Time and date: April 25th, 10:00
Place: Discord

This weeks meeting will be held digitally on Discord.

**Agenda**
1. Status report on the work done so far
2. Plan the work for the final week
3. Plan a final meeting before hand-in of the project

Remember that the final iteration of the project is due April 29th at 15:00!
Also presentation with Ali next week, Tuesday May 3rd 15:00!

Contact this week's meeting organizer if you have any additional points for the agenda, and give notice as soon as possible if you are unable to participate.

Åsulv
Meeting organizer week 17



# Summon group meeting with TA April 28

To: All group members and TA Camilla
Time and date: April 28th, 08:15
Place: Discord

This weeks meeting with the LA will be held digitally on Discord as usual.

**Agenda**
1. Ask questions about the final iteration to the TA

Contact this week's meeting organizer if you have any additional points for the agenda, and give notice as soon as possible if you are unable to participate.

Åsulv
Meeting organizer week 17



# Summon final group meeting April 29

To: All group members
Time and date: April 29th, 08:15
Place: Atriet

This weeks final meeting will be held at campus.

**Agenda**
1. Finish the project
2. Deliver the project on Inspera

This is the final meeting of the project so please attend. Reminder that our project is due April 29th at 15:00!
Also presentation with Ali next week, Tuesday May 3rd 15:00!

Contact this week's meeting organizer if you have any additional points for the agenda, and give notice as soon as possible if you are unable to participate.

Åsulv
Meeting organizer week 17
