/** Squash Tournament Organizer - PROG1004 Project Final
 *
 *  A simple tournament organizer for the sport Squash
 *
 *  To-do list:
 *  - Add comments where needed
 *  - Fix bugs
 *
 *  @file FinalProgram.cpp
 *  @author sulv Vaa
 */


#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <random>
using namespace std;

const int MAXPLAYERS = 16;              ///< Max players in a tournament

float rnd() {
    static std::random_device device;
    static std::mt19937 gen(device());
    static std::uniform_real_distribution<float> dist(0.0F, 1.0F);
    return dist(gen);
}

int rnd(int low, int high) {
    float temp = rnd() * (high - low) + low;
    int itemp = temp;
    if (temp-itemp >= 0.5) {
        itemp++;
    }
    return itemp;
}

/** Class player with relevant information
 *  string name     - The players name
 *  int bracket     - Which bracket the player can be added to a matchup in
 *  bool hasMatchup - Whether the player has a matchup in the bracket
 */
class Player {
    private:
        string name;
        int bracket;
        bool hasMatchup;
    public:
        Player(string n) {
            name = n;
            bracket = 1;
            hasMatchup = false;
        }

        Player() {
            name = "blank";
            bracket = 1;
            hasMatchup = false;
        }

        string getName() {
            return name;
        }

        int getBracket() {
            return bracket;
        }

        bool getHasMatchup() {
            return hasMatchup;
        }

        void setPlayerdata(int brckt, bool hasM) {
            bracket = brckt;
            hasMatchup = hasM;
        }

        void loadData(ifstream & load) {
            char hasM = ' ';
            load >> hasM;   load.ignore();
            if (hasM == 'T') hasMatchup = true;
            else hasMatchup = false;
            load >> bracket;    load.ignore();
            getline(load, name);
        }

        void saveData(ofstream & save) {
            if (hasMatchup) save << "T";
            else save << "F";
            save << " " << bracket << " " << name << "\n";
        }
};

/** Class matchup that contains information about an individual match
 *  Player* player1 - A pointer to player 1 in the match
 *  Player* player2 - A pointer to player 2 in the match
 *  int result      - The result of the match (0 - not finished, 1 - Player 1 won, 2 - Player 2 won)
 */
class Matchup {
    private:
        Player* player1;
        Player* player2;
        int result;
    public:
        Matchup() {
            player1 = nullptr;
            player2 = nullptr;
            result = 0;
        }

        bool isRegistered() {
            return (player1 && player2);
        }

        string getPlayername(bool p2) {
            if(player1 && player2) {
                if(!p2) {
                    return player1->getName();
                } else {
                    return player2->getName();
                }
            } else {
                return "";
            }
        }

        Player* getPlayer(bool p2) {
            if(player1 && player2) {
                if(!p2) {
                    return player1;
                } else {
                    return player2;
                }
            } else {
                return nullptr;
            }
        }

        int getResult() {
            return result;
        }

        void setMatchup(Player* p1, Player* p2) {
            player1 = p1;
            player2 = p2;
        }

        void setResult(int res) {
            result = res;
        }

        void printMatchup() {
            if(player1 && player2) {        // Check if both player pointers are actually pointing to a player object
                cout << player1->getName() << " vs. " << player2->getName() << "\n";
                switch(result) {
                    case 0:
                        cout << "\tResult: Not registered\n";
                        break;
                    case 1:
                        cout << "\tResult: " << player1->getName() << " won!\n";
                        break;
                    case 2:
                        cout << "\tResult: " << player2->getName() << " won!\n";
                        break;
                    default:
                        cout << "\tResult: undefined\n";
                        break;
                }
            } else {
                cout << "Empty matchup\n";
            }
        }

        void loadData(ifstream & load, int tournamentIndex);
        void saveData(ofstream & save, int tournamentIndex);
};

/** Class bracket that contains information about an individual bracket/round in a tournament
 *  vector <Matchup*> matchups - A vector-array of pointers to the matchups in the bracket/round
 */
class Bracket {
    private:
        vector <Matchup*> matchups;
    public:
        Bracket(int matchupCount) {
            for(int i = 0; i < matchupCount; i++) {
                Matchup* newMatchup = new Matchup;
                matchups.push_back(newMatchup);
            }
        }

        ~Bracket() {
            for (int i = 0; i < matchups.size(); i++) {
                delete matchups[i];
            }
            matchups.clear();
        }

        int getMatchupCount() {
            return matchups.size();
        }

        int getMatchupResult(int index) {
            return matchups[index]->getResult();
        }

        bool getMatchupIsRegistered(int index) {
            return matchups[index]->isRegistered();
        }

        string getPlayername(int index, bool p2) {
            return matchups[index]->getPlayername(p2);
        }

        Player* getPlayer(int index, bool p2) {
            return matchups[index]->getPlayer(p2);
        }

        void printMatchups() {
            cout << "\n-= Matchups =-\n";
            for (int i = 0; i < matchups.size(); i++) {
                cout << "[" << i+1 << "] ";
                matchups[i]->printMatchup();
            }
        }

        void setMatchup(int index, Player* p1, Player* p2) {
            matchups[index]->setMatchup(p1,p2);
        }

        void setMatchupResult(int index, int res) {
            matchups[index]->setResult(res);
        }

        void loadData(ifstream & load, int tournamentIndex) {
            for (int i = 0; i < matchups.size(); i++) {
                matchups[i]->loadData(load,tournamentIndex);
            }
        }

        void saveData(ofstream & save, int tournamentIndex) {
            for (int i = 0; i < matchups.size(); i++) {
                matchups[i]->saveData(save,tournamentIndex);
            }
        }
};

/** Main class tournament that contains relevant information about a tournament
 *  string name                - The name of the tournament
 *  vector <Player*> players   - A vector-array of pointers to the players in the tournament
 *  vector <Bracket*> brackets - A vector-array of pointers to the brackets/rounds in the tournament
 *  bool isActive              - Whether the tournament has any matchups, blocks the ability to remove players
 */
class Tournament {
    private:
        string name;
        vector <Player*> players;
        vector <Bracket*> brackets;
        bool isActive;
    public:
        Tournament() {
            name = "blank";
            players.reserve(MAXPLAYERS);
            Bracket* newBracket = new Bracket(8);
            brackets.push_back(newBracket);
            newBracket = new Bracket(4);
            brackets.push_back(newBracket);
            newBracket = new Bracket(2);
            brackets.push_back(newBracket);
            newBracket = new Bracket(1);
            brackets.push_back(newBracket);
            isActive = false;
        }

        Tournament(string n) {
            name = n;
            players.reserve(MAXPLAYERS);
            Bracket* newBracket = new Bracket(8);
            brackets.push_back(newBracket);
            newBracket = new Bracket(4);
            brackets.push_back(newBracket);
            newBracket = new Bracket(2);
            brackets.push_back(newBracket);
            newBracket = new Bracket(1);
            brackets.push_back(newBracket);
            isActive = false;
        }

        ~Tournament() {
            for (int i = 0; i < players.size(); i++) {
                delete players[i];
            }
            players.clear();
            for (int i = 0; i < brackets.size(); i++) {
                delete brackets[i];
            }
            brackets.clear();
        }

        void newPlayer() {
            string n;

            if(players.size() < MAXPLAYERS) {
                cout << "\n-= New player =-\n";
                cout << "Name: ";
                getline(cin, n);
                Player* newPlayer = new Player(n);
                players.push_back(newPlayer);
                cout << "\nNew player " << n << " created for tournament " << name << "!\n";
            } else {
                cout << "\n\tERROR: Max players registered for this tournament!\n";
            }
        }

        void removePlayer(bool all) {
            if(!isActive) {
                if(!players.empty()) {
                    if (!all) {
                        int index;
                        cout << "\n-= Remove player =-\n";
                        printPlayers(true);
                        cout << "Selection (0 to cancel): ";
                        cin >> index;   cin.ignore();
                        if (index >= 1 && index <= players.size()) {
                            cout << "\nPlayer " << players[index-1]->getName() << " removed from tournament " << name << "!\n";
                            delete players[index-1];
                            players[index-1] = players[players.size()-1];
                            players.pop_back();
                        } else if (index != 0) {
                            cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                            index = 0;
                        }
                    } else {
                        char opt = ' ';
                        cout << "\n\tAre you sure? Removed players cannot be restored!\n";
                        cout << "\tALL PLAYERS FOR TOURNAMENT " << name << " WILL BE LOST!\n";
                        cout << "\t[1] Yes\n";
                        cout << "\t[2] No\n";
                        cout << "\tSelection: ";
                        cin >> opt; cin.ignore();
                        if (opt == '1') {
                            for (int i = 0; i < players.size(); i++) {
                                delete players[i];
                            }
                            players.clear();
                        } else {
                            cout << "\n\tDeletion canceled!\n";
                        }
                    }
                } else {
                    cout << "\n\tERROR: No players registered for this tournament!\n";
                }
            } else {
                cout << "\n\tERROR: Unable to remove player(s) from tournament in-progress! Reset all matchups and try again!\n";
            }
        }

        void updateMatchupStatus(int brckt, bool reset=false) {
            if (!reset) {
                for (int i = 0; i < players.size(); i++) {
                    if (players[i]->getBracket() == brckt+1) {
                        players[i]->setPlayerdata(brckt+1,false);
                    }
                }
                for (int i = 0; i < brackets[brckt]->getMatchupCount(); i++) {
                    if (brackets[brckt]->getMatchupIsRegistered(i)) {
                        brackets[brckt]->getPlayer(i,false)->setPlayerdata(brckt+1,true);
                        brackets[brckt]->getPlayer(i,true)->setPlayerdata(brckt+1,true);
                    }
                }
            } else {
                for (int i = 0; i < players.size(); i++) {
                        players[i]->setPlayerdata(1,false);
                }
            }
        }

        /** This function sets matchups via user input in two ways:
         *  - One specified matchup in a specified bracket
         *  - All matchups in a specified bracket
         *
         *  @param bool multi - Whether to set all matchups or a specified matchup
         */
        void setMatchups(bool multi) {
            int bracket = 0;
            int matchup = 0;
            int p1 = 0;
            int p2 = 0;

            char opt = ' ';

            bool validPlayers = false;          // Used to require user to input a valid player combination for every matchup
                                                // when going through an entire bracket

            if (players.size() >= 2) {
                cout << "\n-= Select bracket =-\n";
                cout << "[1] First bracket - First Round\n";
                cout << "[2] Second bracket - Second Round\n";
                cout << "[3] Third bracket - Semi-finals\n";
                cout << "[4] Fourth bracket - Finals\n";
                cout << "Selection (0 is cancel): ";
                cin >> bracket; cin.ignore();

                if (bracket >= 1 && bracket <= 4) {
                    printMatchups(bracket-1);
                    if (multi) {
                        if (getBracketPlayerCount(bracket) >= brackets[bracket-1]->getMatchupCount()*2) {
                            for (int i = 0; i < brackets[bracket-1]->getMatchupCount(); i++) {
                                validPlayers = false;
                                while(validPlayers == false) {
                                    validPlayers = false;
                                    cout << "\n-= Valid players for bracket #" << bracket << " =-\n";
                                    printPlayers(false,bracket);
                                    if (brackets[bracket-1]->getMatchupIsRegistered(i)) {
                                        cout << "\n\tWARNING: This matchup has already been set, any changes will overwrite this matchup\n";
                                    }
                                    cout << "\n-= Set matchup #" << i+1 << " for bracket #" << bracket << " =-\n";
                                    cout << "Player 1 (0 is skip, -1 is cancel): ";
                                    cin >> p1; cin.ignore();
                                    cout << "Player 2 (0 is skip, -1 is cancel): ";
                                    cin >> p2; cin.ignore();
                                    if ((p1 >= 1 && p1 <= players.size()) && (p2 >= 1 && p2 <= players.size()) && p1 != p2) {
                                        brackets[bracket-1]->setMatchup(i, players[p1-1], players[p2-1]);
                                        updateMatchupStatus(bracket-1);
                                        isActive = true;
                                        validPlayers = true;
                                    } else {
                                        cout << "\n\tERROR: Invalid player selection! Matchup not set/changed!\n";
                                        if (p1 != 0 && p1 != -1) {
                                            p1 = 0;
                                        }
                                        if (p2 != 0 && p2 != -1) {
                                            p2 = 0;
                                        }
                                    }
                                    if (p1 == 0 || p2 == 0) {       // 0 is skip, exit the loop
                                        validPlayers = true;
                                    }
                                    if (p1 == -1 || p2 == -1) {     // -1 is cancel, exit both loops
                                        validPlayers = true;
                                        i = brackets[bracket-1]->getMatchupCount();
                                    }
                                }
                            }
                        } else {
                            cout << "\n\tERROR: Not enough valid players to create matchups in this bracket!\n";
                            cout << "\t       " << brackets[bracket-1]->getMatchupCount()*2 << " valid players required, there are currently " << getBracketPlayerCount(bracket) << " valid players.\n";
                        }
                    } else {
                        while(opt != '2') {
                            cout << "Selection (0 is cancel): ";
                            cin >> matchup; cin.ignore();
                            if (matchup >= 1 && matchup <= brackets[bracket-1]->getMatchupCount()) {
                                if (getBracketPlayerCount(bracket) >= 2) {
                                    cout << "\n-= Valid players for bracket #" << bracket << " =-\n";
                                    printPlayers(false,bracket);
                                } else {
                                    cout << "\nNot enough valid players exist for this bracket!\n";
                                    cout << "Please make sure you selected the correct bracket\n";
                                    cout << "-= All players =-\n";
                                    printPlayers(true);
                                }
                                cout << "\n-= Set matchup #" << matchup << " for bracket #" << bracket << " =-\n";
                                cout << "Player 1 (0 is cancel): ";
                                cin >> p1; cin.ignore();
                                cout << "Player 2 (0 is cancel): ";
                                cin >> p2; cin.ignore();
                                if ((p1 >= 1 && p1 <= players.size()) && (p2 >= 1 && p2 <= players.size()) && p1 != p2) {
                                    brackets[bracket-1]->setMatchup(matchup-1, players[p1-1], players[p2-1]);
                                    updateMatchupStatus(bracket-1);
                                    isActive = true;
                                } else {
                                    cout << "\n\tERROR: Invalid player selection! Matchup not set/changed!\n";
                                    if (p1 != 0) {
                                        p1 = 0;
                                    }
                                    if (p2 != 0) {
                                        p2 = 0;
                                    }
                                }

                                if (bracket != 4) {
                                    cout << "\nDo you want to set another matchup in this bracket?\n";
                                    cout << "[1] Yes\n";
                                    cout << "[2] No\n";
                                    cout << "Selection: ";
                                    cin >> opt; cin.ignore();
                                } else {
                                    opt = '2';
                                }
                            } else if (matchup != 0) {
                                matchup = 0;
                                cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                            }

                            switch (opt) {
                                case '1':
                                    printMatchups(bracket-1);
                                    opt = ' ';
                                    break;
                                case '2':
                                    break;
                                default:
                                    opt = '2';
                                    cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                                    break;
                            }
                        }
                    }
                } else if (bracket != 0) {
                    bracket = 0;
                    cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                }
            } else {
                cout << "\n\tERROR: Not enough players registered to create any matchups!\n";
            }
        }

        void randomizeMatchups() {
            int bracket = 0;
            char opt = ' ';

            vector <Player*> validPlayers;

                cout << "\n-= Select bracket =-\n";
                cout << "[1] First bracket - First Round\n";
                cout << "[2] Second bracket - Second Round\n";
                cout << "[3] Third bracket - Semi-finals\n";
                cout << "[4] Fourth bracket - Finals\n";
                cout << "Selection (0 is cancel): ";
                cin >> bracket; cin.ignore();

                if (bracket >= 1 && bracket <= 4) {
                    for (int i = 0; i < players.size(); i++) {
                        if (players[i]->getBracket() == bracket) {
                            validPlayers.push_back(players[i]);
                        }
                    }

                    if (validPlayers.size() >= brackets[bracket-1]->getMatchupCount()*2) {
                        cout << "\n\tAre you sure? Any pre-existing matchups in the bracket will be overwritten!\n";
                        cout << "\t[1] Yes\n";
                        cout << "\t[2] No\n";
                        cout << "\tSelection: ";
                        cin >> opt; cin.ignore();
                        if (opt == '1') {
                            for (int i = 0; i < brackets[bracket-1]->getMatchupCount(); i++) {

                                int p1 = -1;
                                int p2 = -1;

                                p1 = rnd(0,validPlayers.size()-1);
                                p2 = rnd(0,validPlayers.size()-1);
                                while (p1 == p2) {
                                    p2 = rnd(0,validPlayers.size()-1);
                                }

                                while (validPlayers[p1] == nullptr || validPlayers[p2] == nullptr) {
                                    p1 = rnd(0,validPlayers.size()-1);
                                    p2 = rnd(0,validPlayers.size()-1);
                                    while (p1 == p2) {
                                        p2 = rnd(0,validPlayers.size()-1);
                                    }
                                }

                                brackets[bracket-1]->setMatchup(i,validPlayers[p1],validPlayers[p2]);
                                brackets[bracket-1]->setMatchupResult(i,0);

                                validPlayers[p1]->setPlayerdata(bracket,true);
                                validPlayers[p2]->setPlayerdata(bracket,true);

                                validPlayers[p1] = nullptr;
                                validPlayers[p2] = nullptr;
                            }
                            updateMatchupStatus(bracket-1);
                            isActive = true;
                        }
                    } else {
                        cout << "\n\tERROR: Not enough players to randomize matchups for this bracket!\n";
                        cout << "\t       Found " << validPlayers.size() << " valid players, expected " << brackets[bracket-1]->getMatchupCount()*2 << ".\n";
                    }
                } else if (bracket != 0) {
                    bracket = 0;
                    cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                }
        }

        void clearMatchups(bool all) {
            int bracket = 0;
            char opt = ' ';

            if (all) {
                cout << "\n\tAre you sure? Cleared matchups cannot be recovered!\n";
                cout << "\tALL MATCHUPS FOR TOURNAMENT " << name << " WILL BE LOST!\n";
                cout << "\t[1] Yes\n";
                cout << "\t[2] No\n";
                cout << "\tSelection: ";
                cin >> opt; cin.ignore();
                if (opt == '1') {
                    for (int i = 0; i < brackets.size(); i++) {
                        for (int j = 0; j < brackets[i]->getMatchupCount(); j++) {
                            brackets[i]->setMatchup(j, nullptr, nullptr);
                            brackets[i]->setMatchupResult(j, 0);
                        }
                        updateMatchupStatus(i,true);
                    }
                    isActive = false;
                    cout << "\n\tMatchups for tournament " << name << " cleared!\n";
                }

            } else {
                cout << "\n-= Select bracket =-\n";
                cout << "[1] First bracket - First Round\n";
                cout << "[2] Second bracket - Second Round\n";
                cout << "[3] Third bracket - Semi-finals\n";
                cout << "[4] Fourth bracket - Finals\n";
                cout << "Selection (0 is cancel): ";
                cin >> bracket; cin.ignore();

                if (bracket >= 1 && bracket <= 4) {
                    for (int i = 0; i < brackets[bracket-1]->getMatchupCount(); i++) {
                        brackets[bracket-1]->setMatchup(i, nullptr, nullptr);
                        brackets[bracket-1]->setMatchupResult(i, 0);
                    }
                    updateMatchupStatus(bracket-1);
                    if (noRegisteredMatchups()) {
                        isActive = false;
                    }
                    cout << "\n\tMatchups in bracket #" << bracket << " has been cleared for tournament " << name << "!\n";
                } else if (bracket != 0) {
                    bracket = 0;
                    cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                }
            }
        }

        bool noRegisteredMatchups() {
            bool noMatchups = true;

            for(int i = 0; i < brackets.size(); i++) {
                for(int j = 0; j < brackets[i]->getMatchupCount(); j++) {
                    if (brackets[i]->getMatchupIsRegistered(j)) {
                        noMatchups = false;
                    }
                }
            }

            return noMatchups;
        }

        void setResults(bool multi) {
            int bracket = 0;
            int matchup = 0;
            int result = 0;

            char opt = ' ';

            if (isActive == true) {
                cout << "\n-= Select bracket =-\n";
                cout << "[1] First bracket - First Round\n";
                cout << "[2] Second bracket - Second Round\n";
                cout << "[3] Third bracket - Semi-finals\n";
                cout << "[4] Fourth bracket - Finals\n";
                cout << "Selection (0 is cancel): ";
                cin >> bracket; cin.ignore();

                if (bracket >= 1 && bracket <= 4) {
                    printMatchups(bracket-1);
                    if (multi) {
                        for (int i = 0; i < brackets[bracket-1]->getMatchupCount(); i++) {
                            if (brackets[bracket-1]->getMatchupIsRegistered(i)) {
                                if (brackets[bracket-1]->getMatchupResult(i) != 0) {
                                    cout << "\n\tWARNING: The result of this matchup has already been registered, any changes will overwrite it!\n";
                                }
                                cout << "\n-= Set result for matchup #" << i+1 << " in bracket #" << bracket << " =-\n";
                                cout << "[1] Player 1 (" << brackets[bracket-1]->getPlayername(i,false) << ") won\n";
                                cout << "[2] Player 2 (" << brackets[bracket-1]->getPlayername(i,true) << ") won\n";
                                cout << "[3] Reset result for this matchup\n";
                                cout << "[4] Skip this matchup\n";
                                cout << "[5] Cancel\n";
                                cout << "Selection: ";
                                cin >> opt; cin.ignore();

                                switch (opt) {
                                    case '1':
                                        brackets[bracket-1]->setMatchupResult(i,1);
                                        brackets[bracket-1]->getPlayer(i,false)->setPlayerdata(bracket+1,false);
                                        break;
                                    case '2':
                                        brackets[bracket-1]->setMatchupResult(i,2);
                                        brackets[bracket-1]->getPlayer(i,true)->setPlayerdata(bracket+1,false);
                                        break;
                                    case '3':
                                        if (brackets[bracket-1]->getMatchupResult(i) == 1) {
                                            brackets[bracket-1]->getPlayer(i,false)->setPlayerdata(bracket,true);
                                        } else if (brackets[bracket-1]->getMatchupResult(i) == 2) {
                                            brackets[bracket-1]->getPlayer(i,true)->setPlayerdata(bracket,true);
                                        }
                                        brackets[bracket-1]->setMatchupResult(i,0);
                                        break;
                                    case '4':
                                        break;
                                    case '5':
                                        i = brackets[bracket-1]->getMatchupCount();
                                        break;
                                    default:
                                        cout << "\n\tERROR: Invalid selection! Skipping this matchup!\n";
                                        break;
                                }
                            } else {
                                cout << "\n\tMatchup #" << i+1 << " is empty! Skipping...\n";
                            }
                        }
                    } else {
                        opt = ' ';
                        while(opt != '2') {
                            cout << "Selection (0 is cancel): ";
                            cin >> matchup; cin.ignore();

                            if (matchup >= 1 && matchup <= brackets[bracket-1]->getMatchupCount()) {
                                if (brackets[bracket-1]->getMatchupIsRegistered(matchup-1)) {
                                    cout << "\n-= Set result for matchup #" << matchup << " in bracket #" << bracket << " =-\n";
                                    cout << "[1] Player 1 (" << brackets[bracket-1]->getPlayername(matchup-1,false) << ") won\n";
                                    cout << "[2] Player 2 (" << brackets[bracket-1]->getPlayername(matchup-1,true) << ") won\n";
                                    cout << "[3] Reset result for this matchup\n";
                                    cout << "[4] Cancel\n";
                                    cout << "Selection: ";
                                    cin >> opt; cin.ignore();

                                    switch (opt) {
                                        case '1':
                                            brackets[bracket-1]->setMatchupResult(matchup-1,1);
                                            brackets[bracket-1]->getPlayer(matchup-1,false)->setPlayerdata(bracket+1,false);
                                            opt = ' ';
                                            break;
                                        case '2':
                                            brackets[bracket-1]->setMatchupResult(matchup-1,2);
                                            brackets[bracket-1]->getPlayer(matchup-1,true)->setPlayerdata(bracket+1,false);
                                            opt = ' ';
                                            break;
                                        case '3':
                                            if (brackets[bracket-1]->getMatchupResult(matchup-1) == 1) {
                                                brackets[bracket-1]->getPlayer(matchup-1,false)->setPlayerdata(bracket,true);
                                            } else if (brackets[bracket-1]->getMatchupResult(matchup-1) == 2) {
                                                brackets[bracket-1]->getPlayer(matchup-1,true)->setPlayerdata(bracket,true);
                                            }
                                            brackets[bracket-1]->setMatchupResult(matchup-1,0);
                                            opt = ' ';
                                            break;
                                        case '4':
                                            break;
                                        default:
                                            cout << "\n\tERROR: Invaild selection! Result for this matchup not set/changed\n";
                                            break;
                                    }
                                } else {
                                    cout << "\n\tERROR: This matchup is empty!\n";
                                }
                            } else if (matchup != 0) {
                                matchup = 0;
                                cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                            }

                            if (bracket != 4) {
                                cout << "\nDo you want to set another result in this bracket?\n";
                                cout << "[1] Yes\n";
                                cout << "[2] No\n";
                                cout << "Selection: ";
                                cin >> opt; cin.ignore();

                                switch (opt) {
                                    case '1':
                                        printMatchups(bracket-1);
                                        opt = ' ';
                                        break;
                                    case '2':
                                        break;
                                    default:
                                        opt = '2';
                                        cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                                        break;
                                }
                            } else {
                                opt = '2';
                            }
                        }
                    }
                } else if (bracket != 0) {
                    bracket = 0;
                    cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                }
            } else {
                cout << "\n\tERROR: No matchups have been registered yet!\n";
            }
        }

        void resetResults(bool all) {
            int bracket = 0;
            char opt = ' ';

            if (all) {
                cout << "\n\tAre you sure? Reset results cannot be recovered!\n";
                cout << "\tALL RESULTS FOR TOURNAMENT " << name << " WILL BE LOST!\n";
                cout << "\t[1] Yes\n";
                cout << "\t[2] No\n";
                cout << "\tSelection: ";
                cin >> opt; cin.ignore();
                if (opt == '1') {
                    for (int i = 0; i < brackets.size(); i++) {
                        for (int j = 0; j < brackets[i]->getMatchupCount(); j++) {
                            if(brackets[i]->getMatchupResult(j) != 0) {
                                brackets[i]->getPlayer(j,false)->setPlayerdata(i+1, true);
                                brackets[i]->getPlayer(j,true)->setPlayerdata(i+1, true);
                            }
                            brackets[i]->setMatchupResult(j, 0);
                        }
                    }
                    cout << "\n\tAll results reset for tournament " << name << "!\n";
                }

            } else {
                cout << "\n-= Select bracket =-\n";
                cout << "[1] First bracket - First Round\n";
                cout << "[2] Second bracket - Second Round\n";
                cout << "[3] Third bracket - Semi-finals\n";
                cout << "[4] Fourth bracket - Finals\n";
                cout << "Selection (0 is cancel): ";
                cin >> bracket; cin.ignore();

                if (bracket >= 1 && bracket <= 4) {
                    for (int i = 0; i < brackets[bracket-1]->getMatchupCount(); i++) {
                        if(brackets[bracket-1]->getMatchupResult(i) == 1) {
                            brackets[bracket-1]->getPlayer(i,false)->setPlayerdata(bracket, true);
                        } else if(brackets[bracket-1]->getMatchupResult(i) == 2) {
                            brackets[bracket-1]->getPlayer(i,true)->setPlayerdata(bracket, true);
                        }
                        brackets[bracket-1]->setMatchupResult(i, 0);
                    }
                    cout << "\n\tAll results for bracket #" << bracket << " reset in tournament " << name << "!\n";
                } else if (bracket != 0) {
                    bracket = 0;
                    cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                }
            }
        }

        void printMatchups(int bracket) {
            brackets[bracket]->printMatchups();
        }

        void viewMatchups() {
                int bracket = 0;

                if (isActive) {
                    cout << "\n-= Select bracket =-\n";
                    cout << "[1] First bracket - First Round\n";
                    cout << "[2] Second bracket - Second Round\n";
                    cout << "[3] Third bracket - Semi-finals\n";
                    cout << "[4] Fourth bracket - Finals\n";
                    cout << "Selection (0 is cancel): ";
                    cin >> bracket; cin.ignore();
                    if (bracket >= 1 && bracket <= 4) {
                        printMatchups(bracket-1);
                    } else if (bracket != 0) {
                        bracket = 0;
                        cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
                    }
                } else {
                    cout << "\n\tERROR: No matchups registered for this tournament!\n";
                }
        }

        void printInfo() {
            cout << name;
        }

        /** This function prints players to the screen in two ways:
         *  - All players registered to the tournament
         *  - All players that are valid for a given bracket (have the correct bracket value)
         *
         *  @param bool allPlayers - Whether to print all players or only players valid for a given bracket
         *  @param int brckt = 0   - The bracket to check for in the second mode
         */
        void printPlayers(bool allPlayers, int brckt = 0) {
            if (!players.empty()) {
                if (allPlayers) {
                    for(int i = 0; i < players.size(); i++) {
                        cout << "[" << i+1 << "]: " << players[i]->getName();
                        if (players[i]->getBracket() == 5) {
                            cout << " (Winner)\n";
                        } else {
                            cout << " (Bracket #" << players[i]->getBracket() << ((players[i]->getHasMatchup()) ? " | has matchup)" : ")") << "\n";
                        }
                    }
                } else {
                    if (getBracketPlayerCount(brckt) == 0) {
                        cout << "No valid players for this bracket!\n";
                    }
                    for(int i = 0; i < players.size(); i++) {
                        if(players[i]->getBracket() == brckt) {
                            cout << "[" << i+1 << "]: " << players[i]->getName()
                                 << ((players[i]->getHasMatchup()) ? " (has matchup)" : "") << "\n";
                        }
                    }
                }
            } else {
                cout << "\n\tERROR: No players registered for this tournament!\n";
            }
        }

        string getName() {
            return name;
        }

        int getPlayerCount() {
            return players.size();
        }

        int getBracketPlayerCount(int brckt) {
            int amnt = 0;
            for (int i = 0; i < players.size(); i++) {
                if (players[i]->getBracket() == brckt) {
                    amnt++;
                }
            }
            // cout << "\n\tDEBUG: Returning " << amnt << "\n";
            return amnt;
        }

        vector <Player*> getPlayers() {
            return players;
        }

        Player* getSpecificPlayer(int index) {
            return players[index];
        }

        int getPlayerIndex(Player* p) {
            for (int i = 0; i < players.size(); i++) {
                if (p == players[i]) {
                    return i;
                }
            }
            return -1;
        }

        void setPlayerdata(int index, int brckt, bool hasM) {
            players[index]->setPlayerdata(brckt, hasM);
        }

        void loadData(ifstream & load, int tournamentIndex) {
            load.ignore(2);

            char isA = ' ';
            load >> isA;    load.ignore();
            if (isA == 'T') isActive = true;
            else isActive = false;

            getline(load,name);

            load.ignore(2);

            int playerCount = 0;
            load >> playerCount;    load.ignore();
            for (int i = 0; i < playerCount; i++) {
                Player* loadPlayer = new Player;
                loadPlayer->loadData(load);
                players.push_back(loadPlayer);
            }

            load.ignore(2);

            load.ignore(2);
            brackets[0]->loadData(load,tournamentIndex);
            load.ignore(4);
            brackets[1]->loadData(load,tournamentIndex);
            load.ignore(4);
            brackets[2]->loadData(load,tournamentIndex);
            load.ignore(4);
            brackets[3]->loadData(load,tournamentIndex);
        }

        void saveData(ofstream & save, int tournamentIndex) {
            save << "T ";
            if (isActive) save << "T";
            else save << "F";
            save << " " << name << "\n";
            save << "P " << players.size() << "\n";
            for (int i = 0; i < players.size(); i++) {
                players[i]->saveData(save);
            }
            save << "B 0\n";
            brackets[0]->saveData(save,tournamentIndex);
            save << "B 1\n";
            brackets[1]->saveData(save,tournamentIndex);
            save << "B 2\n";
            brackets[2]->saveData(save,tournamentIndex);
            save << "B 3\n";
            brackets[3]->saveData(save,tournamentIndex);
        }
};

vector <Tournament*> gTournaments;

void checkInputErrors();

void printMainMenu();

void newTournament();
void deleteTournament();
void tournamentSelect();
void manageTournament(int index);
void manageTournamentPlayers(int index);
void manageTournamentMatchups(int index);
void manageTournamentResults(int index);
void manageTournamentDelete(int index);

void loadFile();
void saveFile();

int main() {
    char opt = ' ';

    loadFile();

    while (opt != '4') {
        printMainMenu();
        cout << "Selection: ";
        cin >> opt; cin.ignore();

        switch(opt) {
            case '1':
                newTournament();
                opt = ' ';
                break;
            case '2':
                tournamentSelect();
                opt = ' ';
                break;
            case '3':
                deleteTournament();
                opt = ' ';
                break;
            case '4':
                break;
            default:
                cout << "\n\tERROR: Invalid option!\n";
                opt = ' ';
                checkInputErrors();
                break;
        }
    }

    return 0;
}

void checkInputErrors() {
    if (cin.fail()) {
        cout << "\n\tERROR OCCURED WHILE READING INPUT\n";
        cin.clear();
        string buf; getline(cin,buf);
    }
}

void printMainMenu() {
    cout << "\nTournament Organizer\n";
    cout << "-= Main Menu =-\n";
    cout << "[1] Create new tournament\n";
    cout << "[2] Manage existing tournament\n";
    cout << "[3] Delete existing tournament\n";
    cout << "[4] Exit program\n";
}

void newTournament()  {
    string name;
    char opt = ' ';

    cout << "\n-= New tournament =-\n";
    cout << "Tournament name (0 to cancel): ";
    getline(cin, name);
    if (name != "0") {
        Tournament* newT = new Tournament(name);
        gTournaments.push_back(newT);
        saveFile();

        cout << "\nNew tournament " << name << " created!\n";
        cout << "\n[1] Manage new tournament\n";
        cout << "[2] Create another tournament\n";
        cout << "[3] Return to main menu\n";
        cout << "Selection: ";
        cin >> opt; cin.ignore();
        switch(opt) {
            case '1':
                manageTournament(gTournaments.size()-1);
                break;
            case '2':
                newTournament();
                break;
            case '3':
                break;
        }
    }
}

void deleteTournament() {
    int tournament = 0;
    char opt = ' ';

    cout << "\n-= Existing tournaments =-\n";
    if (!gTournaments.empty()) {
        for (int i = 0; i < gTournaments.size(); i++) {
            cout << "[" << i+1 << "]: "; gTournaments[i]->printInfo(); cout << "\n";
        }
        cout << "Selection (0 to cancel): ";
        cin >> tournament;
        if (tournament >= 1 && tournament <= gTournaments.size()) {
            cout << "\n\tAre you sure? Deleted tournaments cannot be restored!\n";
            cout << "\tALL DATA FOR TOURNAMENT " << gTournaments[tournament-1]->getName() << " WILL BE LOST!\n";
            cout << "\t[1] Yes\n";
            cout << "\t[2] No\n";
            cout << "\tSelection: ";
            cin >> opt;
            if (opt == '1') {
                delete gTournaments[tournament-1];
                gTournaments[tournament-1] = gTournaments[gTournaments.size()-1];
                gTournaments.pop_back();
                saveFile();
                cout << "\n\tTournament deleted!\n";
            } else {
                cout << "\n\tDeletion canceled!\n";
            }
        } else if (tournament != 0) {
            tournament = 0;
        }
    } else {
        cout << "\n\tERROR: No tournaments have been registered!\n";
    }
}

void tournamentSelect() {
    int opt = 0;

    cout << "\n-= Existing tournaments =-\n";
    if (!gTournaments.empty()) {
        for (int i = 0; i < gTournaments.size(); i++) {
            cout << "[" << i+1 << "]: "; gTournaments[i]->printInfo(); cout << "\n";
        }
        cout << "Selection (0 to cancel): ";
        cin >> opt;
        if (opt >= 1 && opt <= gTournaments.size()) {
            manageTournament(opt-1);
        } else if (opt != 0) {
            opt = 0;
            cout << "\n\tERROR: Invalid selection! Operation cancelled!\n";
        }
    } else {
        cout << "\n\tERROR: No tournaments have been registered!\n";
    }
}

void manageTournament(int index) {
    char opt = ' ';

    while (opt != '5') {
        cout << "\n-= Manage tournament: " << gTournaments[index]->getName() << " =-\n";
        cout << "[1] Create and manage players (" << gTournaments[index]->getPlayerCount() << "/" << MAXPLAYERS << ")\n";
        cout << "[2] Create and manage matchups\n";
        cout << "[3] Register and display results\n";
        cout << "[4] Delete/reset players, matchups and results\n";
        cout << "[5] Return to main menu\n";
        cout << "Selection: ";
        cin >> opt; cin.ignore();

        switch (opt) {
            case '1':
                manageTournamentPlayers(index);
                opt = ' ';
                break;
            case '2':
                manageTournamentMatchups(index);
                opt = ' ';
                break;
            case '3':
                manageTournamentResults(index);
                opt = ' ';
                break;
            case '4':
                manageTournamentDelete(index);
                opt = ' ';
                break;
            case '5':
                break;
            default:
                cout << "\n\tERROR: Invalid selection!\n";
                opt = ' ';
                checkInputErrors();
                break;
        }
    }
}

void manageTournamentPlayers(int index) {
    char opt = ' ';

    while (opt != '4') {
        cout << "\n-= Manage players for tournament: " << gTournaments[index]->getName() << " =-\n";
        cout << "Registered players: " << gTournaments[index]->getPlayerCount() << "/" << MAXPLAYERS << "\n";
        cout << "\n[1] Create new player\n";
        cout << "[2] View registered players\n";
        cout << "[3] Remove a player\n";
        cout << "[4] Return to tournament management\n";
        cout << "Selection: ";
        cin >> opt; cin.ignore();

        switch (opt) {
            case '1':
                gTournaments[index]->newPlayer();
                saveFile();
                opt = ' ';
                break;
            case '2':
                cout << "\n-= Registered players =-\n";
                gTournaments[index]->printPlayers(true);
                opt = ' ';
                break;
            case '3':
                gTournaments[index]->removePlayer(false);
                saveFile();
                opt = ' ';
                break;
            case '4':
                break;
            default:
                cout << "\n\tERROR: Invalid selection!\n";
                opt = ' ';
                checkInputErrors();
                break;
        }
    }
}

void manageTournamentMatchups(int index) {
    char opt = ' ';

    while (opt != '5') {
        cout << "\n-= Manage matchups for tournament: " << gTournaments[index]->getName() << " =-\n";
        cout << "[1] Set ALL matchups for a bracket\n";
        cout << "[2] Set ONE matchup in a bracket\n";
        cout << "[3] Randomize all matchups for a bracket\n";
        cout << "[4] View matchups in a bracket\n";
        cout << "[5] Return to tournament management\n";
        cout << "Selection: ";
        cin >> opt; cin.ignore();

        switch (opt) {
            case '1':
                gTournaments[index]->setMatchups(true);
                saveFile();
                opt = ' ';
                break;
            case '2':
                gTournaments[index]->setMatchups(false);
                saveFile();
                opt = ' ';
                break;
            case '3':
                gTournaments[index]->randomizeMatchups();
                saveFile();
                opt = ' ';
                break;
            case '4':
                gTournaments[index]->viewMatchups();
                opt = ' ';
                break;
            case '5':
                break;
            default:
                cout << "\n\tERROR: Invalid selection!\n";
                opt = ' ';
                checkInputErrors();
                break;
        }
    }
}

void manageTournamentResults(int index) {
    char opt = ' ';

    while (opt != '4') {
        cout << "\n-= Manage results for tournament: " << gTournaments[index]->getName() << " =-\n";
        cout << "[1] Set the result of ONE matchup in a bracket\n";
        cout << "[2] Set the result of ALL matchups in a bracket\n";
        cout << "[3] View results in a bracket\n";
        cout << "[4] Return to tournament management\n";
        cout << "Selection: ";
        cin >> opt; cin.ignore();

        switch (opt) {
            case '1':
                gTournaments[index]->setResults(false);
                saveFile();
                opt = ' ';
                break;
            case '2':
                gTournaments[index]->setResults(true);
                saveFile();
                opt = ' ';
                break;
            case '3':
                gTournaments[index]->viewMatchups();
                opt = ' ';
                break;
            case '4':
                break;
            default:
                cout << "\n\tERROR: Invalid selection!\n";
                opt = ' ';
                checkInputErrors();
                break;
        }
    }
}

void manageTournamentDelete(int index) {
    char opt = ' ';

    while (opt != '7') {
        cout << "\n-= Delete/reset data for tournament: " << gTournaments[index]->getName() << " =-\n";
        cout << "[1] Remove a player\n";
        cout << "[2] Remove all players\n";
        cout << "[3] Clear all matchups in a bracket\n";
        cout << "[4] Clear all matchups in all brackets\n";
        cout << "[5] Reset all matchup results in a bracket\n";
        cout << "[6] Reset all matchup results in all brackets\n";
        cout << "[7] Return to tournament management\n";
        cout << "Selection: ";
        cin >> opt; cin.ignore();

        switch (opt) {
            case '1':
                gTournaments[index]->removePlayer(false);
                saveFile();
                opt = ' ';
                break;
            case '2':
                gTournaments[index]->removePlayer(true);
                saveFile();
                opt = ' ';
                break;
            case '3':
                gTournaments[index]->clearMatchups(false);
                saveFile();
                opt = ' ';
                break;
            case '4':
                gTournaments[index]->clearMatchups(true);
                saveFile();
                opt = ' ';
                break;
            case '5':
                gTournaments[index]->resetResults(false);
                saveFile();
                opt = ' ';
                break;
            case '6':
                gTournaments[index]->resetResults(true);
                saveFile();
                opt = ' ';
                break;
            case '7':
                break;
            default:
                cout << "\n\tERROR: Invalid selection!\n";
                opt = ' ';
                checkInputErrors();
                break;
        }
    }
}

void loadFile() {
    ifstream save;
    save.open("TO_SAVED_DATA.dta");

    int tournamentCount = 0;

    if (save) {
        save >> tournamentCount;    save.ignore();
        for (int i = 0; i < tournamentCount; i++) {
            Tournament* loadTournament = new Tournament;
            gTournaments.push_back(loadTournament);
            gTournaments[i]->loadData(save, i);
        }
        cout << "\n\tLoaded " << gTournaments.size() << " tournaments from file!\n";
    } else {
        cout << "\n\tERROR: Could not load from file 'TO_SAVED_DATA.dta'!\n";
    }
}

void saveFile() {
    ofstream save;
    save.open("TO_SAVED_DATA.dta");

    if (save) {
        cout << "\n\tSaving data to file...\n";
        save << gTournaments.size() << "\n";
        for (int i = 0; i < gTournaments.size(); i++) {
            gTournaments[i]->saveData(save,i);
        }
        cout << "\tData saved to file!\n";
    } else {
        cout << "\n\tERROR: Could not save to file 'TO_SAVED_DATA.dta'!\n";
    }
}

void Matchup::loadData(ifstream & load, int tournamentIndex) {
    int p1_index = -1;   int p2_index = -1;
    load >> p1_index;   load.ignore();  load >> p2_index;   load.ignore();  load >> result; load.ignore();

    if (p1_index != -1 && p2_index != -1) {
        player1 = gTournaments[tournamentIndex]->getSpecificPlayer(p1_index);
        player2 = gTournaments[tournamentIndex]->getSpecificPlayer(p2_index);
    } else {
        player1 = nullptr;  player2 = nullptr;
        result = 0;
    }

}

void Matchup::saveData(ofstream & save, int tournamentIndex) {
    int p1_index = gTournaments[tournamentIndex]->getPlayerIndex(player1);
    int p2_index = gTournaments[tournamentIndex]->getPlayerIndex(player2);

    save << p1_index << " " << p2_index << " " << result << "\n";
}
