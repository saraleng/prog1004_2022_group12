## Weekly status report: Even

There were weekly group meetings, and from week 9 there were also weekly TA meetings.

### Week 4-7

Apart from following the teaching of the course, the only thing we produced for the project as the collaboration agreement

### Week 8

Project kick-off. Worked on the first iteration of the wireframe and on the visiond document.

### Week 9

Finised wireframes and continued with vision doc.

### Week 10

Attended the DEVOPS-workshop

### Week 11

Wrote pseudo-code and began work on second iteration of wireframes. Attended the first client meeting.

### Week 12

Finished second wireframe. Integrated feedback from first user test.

### Week 13

Only attended meetings

### Week 14

Attended second client meeting, and attended extra-ordinary group meeting to process feedback from client meeting.

### Week 15

No meetings. Research for main report, particularly software engineering theory.

### Week 16

Began work in earnest on the main report.

### Week 17
Wrote the majority of the main report, including a substantial rewrite of the theory section. Finished the project