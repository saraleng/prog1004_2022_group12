# Weekly Status Report: Sara

### Week 4-7

Wrote the first project report and the collaboration agreement, together with the rest of the group. 


### Week 8

2/21 Planning, early work on how to set up the project.  


### Week 9

2/28 Team meeting with team to discuss and plan this weeks work towards the first iteration, and to contact TA for setting up meeting with her. Responsibility for the first iteration: Usability testing, and vision document.  

3/3 First meeting with TA. TA answered our questions regarding the mandatory documents/tasks (see minutes). Agreed on weekly meetings with TA each Thursday.  

I used the lecture notes to read up on user testing (how to execute and document it), &before planning the usability test with Britt-Åse, and scheduling a time with the participant.  

3/4 Conducted the usability test and wrote the report.  

I spent some time learning to use gitlab (cloning, pushing, etc).  

3/5 Started writing the Vision document together with the other team members.  

3/6 Deadline: Put together and delivered the first iteration.  



### Week 10

3/7 Team meeting discussing the results of the user test, before starting to write the source code next week.  
 
3/10 Meeting with TA. Brief feedback on the first iteration (we will get more feedback next week). Camilla answered questions about the Vision Document, since this needed a lot more work. Discussed MVP and first version requirements of WIKI for the second iteration. Went through how to prepare for the client meeting next week.  

Because of the DevOps workshop, no more work was done on the project this week.

Since I am responsible for organizing the meetings next week (11), I asked the other group members for agenda suggestions and sent out a meeting summon for the meeting on Monday 7th March.  


### Week 11

I was responsible for organizing the meetings this week (and writing minutes), including sending out meeting summon to the client.   

3/14 Team meeting: Writing pseudocode/datastructure together. Planning the next two weeks before delivering the second iteration. Delegated tasks: I am again responsible for the usability test, as well as setting up the Wiki on gitlab. Lastly we prepared for the client meeting. 

First client meeting with Ali. We were reminded that the focus of the course is documentation, not programming. Got feedback on what we have done so far. For the next iteration we need to:
* Include agenda in meeting summon, 
* user test needs more than one test person and should not include their name, 
* we need to work more on the Vision Document, 
* implement 'edit' feature in the program, 
* and use the issue board on gitlab more actively.  


3/17 Weekly meeting with TA, with more feedback on the first iteration. The main thing we have to work on is the Vision Document. Agreed to reschedule weekly TA meetings to 08.15 (still Thursdays). 

I worked on setting up the the WIKI page, and organizing the git repository. And also meda template for minutes and summons (markdown) and uploaded this weeks minutes and summons to git.



### Week 12

3/21 Team meeting: Status report on what we have done since the last meeting, and making sure everything will be done for the second iteration 25th of March (Friday). The MVP and Wireframes are almost done, but the Vision Document need more work (TA was contacted for help). I will continue to work on the WIKI page and usability test. Use case and sequence diagram has not been started: We agreed that if someone finishes their task(s), they can work on the use case and sequence diagram.  

3/22 Planned the next usability test and wrote description. Contacted four participants and scheduled test time.  

3/23 I started working on the use case and sequence diagram, using the lecture notes to understand UML, and online guides, but needed to clear some things up with the TA. 

3/24 Meeting with TA: Used most of the meeting to discuss and ask questions about the Vision Document. Camilla also answered some questions on how to structure the sequence diagram regarding the lifelines.  

Conducted the usability test, before I wrote the report from the test.  

Worked more on setting up the WIKI correctly, because I had some issues figuring out how to get the sidebar to coincide with the WIKI pages.  

3/25 Continued to work on the version 1 of the sequence diagram and the use case diagram.  

Delivered second iteration.  

We agreed on a date and time for final presentation: May 3rd, 15:00



### Week 13

The team meeting this week was held in conjunction with (after) the TA meeting, so that we could discuss the feedback on the second iteration.  

3/31 Meeting with TA, with feedback on second iteration. The feedback applying to my contributions to the second iteration:  
* We need to change the layout of status reports
* WIKI pages needs more work
* Less complex/messy use case diagram 
* Separate between messages and reply messages in sequence diagram (and add more alternatives)  

3/31 Team meeting: Discussed the feedback, and delegated tasks for the week. I will continue to work on the diagrams, and the WIKI.  

4/1-2 Started to work on version 2 of the sequence diagram. Had to study the code in more detail, and realized that the first version of the sequence diagram was too simplified/incorrect.  


### Week 14

The team meeting this week was held in conjunction with (after) the TA meeting, and after the client meeting.  

4/7 Meeting with TA: Additional feedback on the second iteration. Camilla also answered our questions regarding the client meeting: We should start by presenting the MVP, and then the rest of our work briefly.  After the client meeting the team planned how to present the project on the client meeting. 

Meeting with client: We were not well enough prepared, and it took way to long before we managed to show the MVP. The rest of the meeting was used for feedback and guidance on how to prepare for and give a presentation: All documents/presentation should be on one computer, and we have to make sure that all technical equipment work (have a plan b). We also discussed what our priorities should be entering the last weeks of the project: The theory part of the main report is important. Ali also demonstrated how the messages and reply messages in a sequence diagram works, so that I could understand it.  

Second team meeting: We assessed the client meeting; firstly, we need to use more time to plan the final presentation. Secondly, everyone needs to work more on the project. We delegated the task for the next week. I will continue working on version 2 of the use case and sequence diagrams, as well as the Vision Document.  

4/8 Worked on the Vision Document, specifically the section for product features.  

To get a better understanding of software development documentation, both in general, and the specific requirements for this project, I read the lecture notes, and some additional sources. I also read more into how gitlab and WIKI works.  


### Week 15

No meetings this week due to easter holiday, but all team members are available online, and continues to work on their tasks.  

4/12 Based on the feedback on the first version of the use case diagram, I made a more accurate and readable version. I also finished the use case descriptions.  

4/13-15 Wrote several sections in the Vision Document. The sections for risk analysis and applicable standard required more time and research. Especially, I had to get more familiar with the Norwegian laws on universal design, and the WCAG standard, and test the program according to the test procedures for the success criteria, reporting back to the team about changes that we should make to comply with the standard. Some of the sections in the Vision Document has to be finalized when the final source code is ready, and I am also waiting for answers from Camilla (TA) (which will come when the holiday is over).  


### Week 16

I was responsible for organizing the meetings this week (and writing minutes), including sending out meeting summon to the client.   

The team meeting this week was held in conjunction with (before) the TA meeting.  

4/19 Scheduled time with the participants for the final usability test. And finilized the structuring of the WIKI, and started to fill in the pages.  

4/21 Team meeting: Everyone gave an update on what they had done the last week, and what needed more work. All delegated tasks are in progress (a few are finished), but some rescheduling of the tasks/work was needed, mainly regarding things that all team members must contribute to: 
* The source code is updated/error corrected, but must be thoroughly tested for (more) bugs. Everyone must test the program, and study the source code. 
* The main report is in progress, but all team members must contribute on chapter 3, 4 and 5. 
* The Vision Document still needs more work.  

&nbsp;&nbsp;&nbsp;&nbsp;Meeting with TA: Mainly discussing the Vision Document, and went through the requirements for the final deadline. We need to start uploading all files to the git repository, and continue to fill out the WIKI pages. Got some last feedback on the use case diagram, which is missing some includes/excludes.  

4/22 Ran the third user test, and wrote the report. The main problem was input assistance, which Åsulv fixed.  



### Week 17

4/25 Team meeting: After a quick status report, we made a detailed plan for everything that needs to be done until we have finished the whole project, including the presentation. All remaining tasks were delegated. Everything needs to be finished by Thursday, so that we can meet on Friday morning to proofread and go over the whole project one last time. After delivering the project, we will make the presentation.  

Finished the sequence and use case diagram, including them in the WIKI.   

4/26 Ran system testing on all the features of the program. Reported back to the team about some minor incoherences, which are now fixed.  

4/27 Organized the git repository, uploading a template for the status report, and worked on the self reflection document. Continued to fill in the WIKi pages, and worked with Britt-Åse on understanding how to properly make the Class Diagram. Wrote on the main report, specifically about user testing, and results.

4/28 Meeting with TA, clearing up some final questions on the class diagram. Camilla (TA) made us aware that the WIKI should include images of the diagram, manuals, etc.  

Team meeting: Going through the vision document, and the main report.

The rest of the day was used to write on the main report, finishing the self reflection document, and filling out the pages in the WIKI. 

4/29 Final proofreading, and putting together the documents to deliver in Inspera.
