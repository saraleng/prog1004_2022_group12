## Weekly status report: Åsulv


### Week 4-7

Collaborated with the group members on the Collaboration Agreement

### Week 8

(1.5h) Team meeting - Administration
- Early meeting on how to begin work on the project

Total hours this week:  1.5h
### Week 9

(1.5h) Team meeting - Administration
- Meeting on beginning work on the first iteration

(0.8h) Meeting with LA - Administration
- Discussion with LA on the first iteration

(2.0h) Work on domain model - Documentation
- Began work on the domain model for the first iteration

Total hours this week:  4.3h
### Week 10

(1.0h) Team meeting - Administration
- Meeting on progress on the first iteration

(23.0h) DEVops Workshop - Documentation
- Three days of DEVops Workshop meant for us to improve knowledge

(0.5h) Meeting with LA - Administration
- Discussion with LA on the first iteration

Total hours this week: 24.5h
### Week 11

(3.0h) Team meeting - Administration
- Meeting on beginning work on the second iteration

(0.5h) Client meeting - Quality assurance
- Meeting with the client on the work done on the first iteration

(0.6h) Meeting with LA - Administration
- Discussion with LA on the second iteration

(4.8h) Coding the MVP - MVP
- Began coding the Minimum Viable Product

Total hours this week:  8.9h
### Week 12

(2.6h) Team meeting - Administration
- Meeting on the progress on the second iteration

(0.5h) Project reporting - Documentation

(1.8h) Coding the MVP - MVP
- Continued coding the Minimum Viable Product

Total hours this week:  4.8h
### Week 13

(0.8h) Team meeting - Administration
- Meeting on the progress on the second iteration

(1.8h) Project work

Total hours this week:  2.6h
### Week 14

(1.0h) Team meeting - Administration
- Meeting on beginning work on the final iteration

(0.4h) Client meeting - Quality assurance
- Meeting with the client on the work done on the second iteration

(1.0h) Meeting with LA - Administration
- Discussion with LA on the final iteration

(3.0h) Coding the final program - Product
- Began coding the features we decided we would add to the MVP for our final program

Total hours this week:  5.4h
### Week 15

(2.5h) Coding the final program - Product
- Continued coding the program

Total hours this week:  2.5h
### Week 16

(5.8h) Coding the final program - Product
- Finished coding the new features
- Bug fixes and "polish"

(2.0h) Meeting with LA - Administration
- Discussion with LA on the final iteration

Total hours this week:  7.8h
### Week 17

(7.5h) Writing installation and user guides - Documentation
- Wrote the user & installation manuals
- Converted manually from Word to GitLab Wiki "Markdown"
- Some minor fixes on the final program

(0.2h) Minor fixes on Vision Doc - Documentation
- Wrote the correct system requirements for the program

(0.5h) Writing the status report - Documentation
- Collected all time & status reports into this document

(1.0h) Writing the self-reflection - Documentation
- Wrote the self-reflection and handed it in on Blackboard

(4.0h) Final group meeting - Administration
- Collaborated with the team members on finishing and handing in the project

Total hours this week: 13.2h
## Total hours

Total hours on the project: 75.4h
Average hours per week    :  7.5h
